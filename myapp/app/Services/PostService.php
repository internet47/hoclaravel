<?php

namespace App\Services;
use App\Post;
use App\Repositories\PostRepository;
use Illuminate\Http\Request;

class PostService
{
	protected $post;
	function __construct(PostRepository $post)
	{
		return $this->post = $post;
	}

	public function index()
	{
		return $this->post->getAll();
	}


	public function read($id)
	{
		return $this->post->findID($id);
	}

	public function update($id, Request $request)
	{
		$data = $request->all();
		return $this->post->update($id, $data);
	}

	public function delete($id)
	{
		return $this->post->delete($id);
	}

	public function create(Request $request)
	{
		$data = $request->all();
		return $this->post->create ($data);
	}


}//end