<?php
namespace App\Services;
use App\Article;
use App\Repositories\ArticleRepository;
#use App\Http\Requests\ArticleRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
/**
 * ArticleSerice
 */
class ArticleService
{

	protected $articleservice;

    /**
     * Inject Article Repository
     */
    public function __construct(ArticleRepository $articleservice)
    {
        $this->articleservice = $articleservice;
    }


    public function getArticles()
    {
    	return $this->articleservice->getArticles();
    	
    }

    public function createArticle(Request $data, $filename)
    {	
    	$data = $data->all();
    	return $this->articleservice->createArticle($data, $filename);
    }


    public function findArticle($id)
    {
    	return $this->articleservice->findArticle($id);
    }


    public function updateArticle($id, array $data)
    {
    	return $this->articleservice->updateArticle($id,$data);
    }


    public function deleteArticle($id)
    {
        $article = $this->articleservice->findArticle($id);
        if($article)
        {
            $image_file = $article->image_file;
            $folder_upload = config('myconfig.folder_upload', $default = null);
            $file = $folder_upload.DIRECTORY_SEPARATOR.$image_file;

             if (file_exists($file))
                @unlink($file);
            $this->articleservice->deleteArticle($id);

        	return TRUE;
        }
        return ;
    }

    public function isUpload(Request $request)
    {
    	/**
    	 * Initial data
    	 * @var [type]
    	 */
    	$max_size = (int)ini_get('upload_max_filesize') * 1000;
    	$folder_upload = config('myconfig.folder_upload', $default = null);
		$extension      = array('jpg','png', 'gif');
		if ($request->hasFile('image_file'))
		{
			$file           = $request->file('image_file');
			$file_size      =  $file->getSize();
			$file_size = round($file_size / 1024,2);
			$full_file_name      =  $file->getClientOriginalName();  
			$file_extension = strtolower($file->getClientOriginalExtension());
			$name = pathinfo($full_file_name, PATHINFO_FILENAME);

			$filename= $this->generateFilename($name, $file_extension);
			while (file_exists($folder_upload.'/'.$filename))
			{
				$filename= $this->generateFilename($name, $file_extension);
			}


			/**
			 * Check extension and file_size > 0
			 */
			if (($file_size > 0 && $file_size <= $max_size) && in_array($file_extension, $extension))
			{
				$file->move($folder_upload, $filename);
                if(file_exists($folder_upload.'/'.Session::get('image_file_old'))){
                    @unlink($folder_upload.'/'.Session::get('image_file_old'));
                }

				return $filename;
			}
		}
		return '';
    }

    public function generateFilename($file_name, $extenstion)
    {
    	if (!empty($file_name) && !empty($extenstion))
    		return str_slug($file_name).'-'.time().rand(0,999999).'.'.$extenstion;
    	return '';
    }

    public function showArticle($id)
    {
        return $this->articleservice->showArticle($id);
        
    }

}

