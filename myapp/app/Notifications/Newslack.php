<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\SlackMessage;

class Newslack extends Notification
{
    use Queueable;
    protected $message;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($message)
    {
        $this->message = $message;
    }

    /**
     * 
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['slack']; //Phương tiên để thông báo. Ở đây dùng slack, có thể truyền 1 array các phương tiện như mail, slack....
    }

    /**
     * Send notification to Slack
     * @param  [type] $notifiable [description]
     * @return [type]             [description]
     */
    public function toSlack($notifiable)
    {
        //return (new SlackMessage)->content($this->message);
        $url = url('/exceptions/1');

    // return (new SlackMessage)
    //             ->success()
    //             ->content('One of your invoices has been paid!')
    //             ->attachment(function ($attachment) use ($url) {
    //                 $attachment->title('Invoice 1322', $url)
    //                            ->fields([
    //                                 'Title' => 'Server Expenses',
    //                                 'Amount' => '$1,234',
    //                                 'Via' => 'American Express',
    //                                 'Was Overdue' => ':-1:',
    //                             ]);
    //             });

    return (new SlackMessage)
                ->from('Ghost', ':ghost:')
                ->to('#general') //Cấu hình channel cần gửi đến #general #random ...
                ->content('This will be sent to #other');

    }//end toslack
    

   
}
