<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
//Add library of telegram
use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;

class NewTelegram extends Notification
{
    use Queueable;
    protected $message;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($message)
    {
        $this->message = $message;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [TelegramChannel::class];
    }


    public function toTelegram($post)
    {
        return TelegramMessage::create()
                ->to('-234577522') //Đây là tên của channel mà bạn muốn gửi tin nhắn đến: t.me/vietlaravel thì dùng @vietlaravel, còn nếu dùng chat_id thì dùng Vd:-1564564564
                ->content($this->message)->button('Click here','http://test.com');
    }

}
