<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\ArticleService;

class CountUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'count:user {user}';
    protected $article='';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Count the total number of user registered today';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ArticleService $article)
    {
        parent::__construct();
        $this->article = $article;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $userId = $this->argument('user');
        $data = $this->article->getArticles();
        $data = compact('data');
        var_dump($data );
        echo $userId;
        echo "This is a test output message";
    }
}
