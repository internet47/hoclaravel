<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Phone extends Model
{
    protected $fillable = ["phone"];
    protected $primaryKey ='user_id';

    public function user()
    {
    	return $this->belongsTo('App\User','user_id','id');// user_id là foreign key sẽ refer tới id của bảng User
    }

}
