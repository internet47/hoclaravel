<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\APIBaseController as APIBaseController;
use App\Services\ArticleService;
use App\Http\Requests\ArticleRequest;
use App\Article;
use Illuminate\Http\Request;

/**
 * Controller for Article API
 */
class ArticleAPIController extends APIBaseController
{
	protected $article;
	public function __construct(ArticleService $article)
	 {
	 	$this->article = $article;
	 }	 

	public function index()
	{
		$data= $this->article->getArticles();
		return $this->sendResponse($data, 'Bạn đã insert thành công');
		
	}

	public function store(ArticleRequest $article)
	{

		$data = $article->all();
		$article = new Article();
		$article['title']= $data ['title'];
		$article['short_content'] = $data['short_content'];
		$article['content'] = $data['content'];
		$article['image_file'] = $data['image_file'];
		$article['user_id'] = $data['user_id'];
		$article->save();

	return $this->sendResponse($article,'Tạo thành công');
	}

	public function show($id)
	{
		$article = $this->article->findArticle($id);
			return $this->sendResponse($article, "Dữ liệu trả về thành công");
	}

	public function update(ArticleRequest $article, $id)
	{
		$data = $article->all();
		$article = $this->article->findArticle($id);
		if (is_null($article))
			return $this->sendError('Article not found', 'not found');
		$article['title']= $data ['title'];
		$article['short_content'] = $data['short_content'];
		$article['content'] = $data['content'];
		$article['image_file'] = $data['image_file'];
		$article['user_id'] = $data['user_id'];
		$article->save();
	    return	$this->sendResponse($article, "Cập nhập thành công");

	}

	public function delete($id)
	{
		$article = $this->article->findArticle($id);
		if (is_null($article))
			return $this->sendError('Article not found', 'not found');

		$article->delete();
		return	$this->sendResponse($article, "bạn đã xóa thành công ID:".$id);
	}


}