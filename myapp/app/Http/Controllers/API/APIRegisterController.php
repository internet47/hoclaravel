<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller as Controller;
use Illuminate\Http\Request;
use App\User;
use JWTFactory;
use JWTAuth;
use Validator;
use Response;

class APIRegisterController extends Controller
{
    public function register(Request $request)
    {
    	$validation = Validator::make($request->all(), [
			'name'     => 'required',
			'email'    => 'required|string|email|unique:users',
			'password' => 'required'
    		]);

    	if ($validation->fails()){
    		return response()->json($validation->errors(), 400);
    	}
    	$user = User::create([
			'name'     => $request->get('name'),
			'email'    => $request->get('email'),
			'password' =>  bcrypt($request->get('password')),
    	]);

    	//$user = User::first();
    	$token = JWTAuth::fromUser($user);

    	return response()->json(compact('token'), 201);
    }
}
