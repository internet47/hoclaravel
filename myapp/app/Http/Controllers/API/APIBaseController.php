<?php
/**
 * This is API Common for Article
 */

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;


class APIBaseController extends Controller
{
   		
		/**
		 * Send response
		 * @param  [array] $data    [description]
		 * @param  [string] $message [description]
		 * @return [type]          [description]
		 */
   		public function sendResponse($data, $message)
   		{
   			$response = array(
					'success' => true,
					'data'    => $data->toArray(),
					'message' => $message
   			);
   			return response()->json($response, 200);
   		}

   		/**
   		 * Send an  error when json has error
   		 *
   		 * @param      <type>   $error         The error
   		 * @param      array    $errormessage  The errormessage
   		 * @param      integer  $code          The code
   		 *
   		 * @return     <type>   ( description_of_the_return_value )
   		 */
   		public function sendError($error,  $errormessage, $code = 404)
   		{
   			$response = array(
					'success' => false,
					'message' => $error
   			);

   			if (!empty($errormessage))
   				$response['data'] = $errormessage;

   			return response()->json($response, $code);
   			
   		}


}//end 