<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Excel; // đưa facede của Excel vào
use App\Item;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return View('items.items');
    }

    

   /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function import(Request $request)
    {
      if($request->file('imported-file'))
      {
                $path = $request->file('imported-file')->getRealPath();
                $data = Excel::load($path, function($reader) {})->get(); // Đọc file excel/csv sau đó đưa dòng đầu tien làm header và các dòng còn lại làm data
            if(!empty($data) && $data->count())
            {
            $data = $data->toArray();
                for($i=0;$i<count($data);$i++)
                {
                  $dataImported[] = $data[$i]; // Tạo array data để chuẩn bị insert multiple
                }
            }

          Item::insert($dataImported); // insert multiple data
      }
        return back();
  }


  public function importWithDifferenceHeader(Request $request)
    {
      if($request->file('imported-file'))
      {
                $path = $request->file('imported-file')->getRealPath();
                $data = Excel::load($path, function($reader)
          {
                })->get();

          if(!empty($data) && $data->count())
          {
            foreach ($data->toArray() as $row)
            {
              if(!empty($row))
              {
                $dataArray[] =
                [  // Gán cứng từng dữ liệu ứng với colum_name trong database
                  'item_name' => $row['name'],
                  'item_code' => $row['code'],
                  'item_price' => $row['price'],
                  'item_qty' => $row['num1'],
                  'item_tax' => $row['num2'],
                  'item_status' => $row['num3'],
                  'created_at' => $row['day']
                ];
              }
          }
          if(!empty($dataArray))
          {
             Item::insert($dataArray);
             return back();
           }
         }
       }
    }

  public function export()
  {
        $filename = 'item-'.date('Y-m-d-h-i-s-',time()). rand(0,999999);
        $type='xls'; // xsl hoac csv
        $items = Item::all();
        Excel::create($filename, function($excel) use($items) {
          $excel->sheet('ExportFile', function($sheet) use($items) {
              $sheet->fromArray($items);
          });
      })->export($type);

  }


   


}//end class
