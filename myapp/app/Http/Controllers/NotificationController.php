<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Events\MyNotificationEvent;

class NotificationController extends Controller
{

	public function showNotification()
	{
		return View('hello');
	}

    public function sendNotification()
    {
    	$data = 'Hello Viet: ' . rand(0,999);
    	try {
    		event(new MyNotificationEvent($data));
    	return "Message has been sent";
    	} catch (Exception $e) {
    		throw new Exception('Can not send');
    	}
    	
    }
}
