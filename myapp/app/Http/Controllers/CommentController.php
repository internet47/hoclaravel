<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use App\Events\MyNotificationEvent;

class CommentController extends Controller
{
    public function showComment($id)
    {
    	if(!empty($id) && is_numeric($id))
    	{
    		\Session::put('article_id', $id);
    		return View('comments.add')->with('article_id', $id);
    	}
    	return redirect()->route('index.article')->with(['status'=>'Your ID is not compare. Please check again']);
    }


    public function addComment(Request $request)
    {
    	$article_id = \Session::get('article_id');
    	$data = $request->all();
	   	if ($article_id == $data['article_id'])
			{
				$comment = new Comment();
				$comment['content'] = $data['content'];
				$comment['article_id'] = $data['article_id'];
				$comment->save();
				if (\Session::get('article_id'))
					\Session::forget('article_id');
			}
		event(new MyNotificationEvent('Một comment mới trên bài viết:'. $data['article_id']. '. Với nội dung: <br />'.'<strong>'. str_limit($data['content'], 20).'</strong>'));
    	return redirect()->route('index.article')->with(['status'=>'You have just add comment success']);
    }
}
