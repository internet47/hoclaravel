<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\PostService;
use App\Http\Requests\PostRequest;

class PostController extends Controller
{
	protected $postservice;

	public function __construct(PostService $post)
	{
		$this->postservice = $post;
	}


    public function index()
    {
    	$posts = $this->postservice->index();
    	return view ('posts.index', compact('posts'));
    }

    public function createPost (PostRequest $request)
    //public function createPost (Request $request)
    {
    	$this->postservice->create($request);
    	return back()->with(['status'=>'Post created successfully']);
    }

    public function editPost($id)
    {
    	$post=  $this->postservice->read($id);
    	return view ('posts.edit', compact('post'));
    }

    public function updatePost($id, PostRequest $request)
    {
    	$this->postservice->update($id, $request);
    	return redirect('/')->with(['status'=>__('Post updated successfully')]);
    }

    public function deletePost($id)
    {
    	$this->postservice->delete($id);
    	return back()->with(['status'=>'Post deleted successfully']);
    }

}
