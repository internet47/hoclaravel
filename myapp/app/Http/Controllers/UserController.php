<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Phone;
use DB;

class UserController extends Controller
{

	public function index()
	{
		//echo 'asdadasdasd';
		//Show all
			//$user = User::find(1)->phone;
			//dd($user);

		//Add child table
			// $user             = User::find(1);
			// $phone = new Phone();
			// $phone->name='Samsung';
			// $phone->telephone='123456789';
			// $user->phone()->save($phone);

			//$user = User::find(1)->phone;
			//$user  = User::with('phone')->find(1);//get data from 2 tables with id =1
			//$user = User::find(1)->phone->get();
			//$user = DB::select('select * from users where id = ?', [1]);
			
			//QueryBuilder trả về 1 object
				// $users = DB::table('users')->get();
				// dump($users);
				// foreach ($users as $user) {
				// 	echo $user->name;
				// }

			//Eloquent
				// $users= User::all();
				// dump($users);
				// foreach ($users as $user) {
				// 	echo $user->getOriginal('email');
				// }

			// $users = User::chunk(1, function ($users) {
			// 			dump($users);
			// 			    // foreach ($users as $user) {
			// 			    // 	echo $user->name;
			// 			    // }
			// });		


			//$users = User::with('phone')->get();		
			//$users = DB::table('users')->where('id',1)->select('name','email','created_at')->get();    
			// $users = DB::table('users')
   //          ->where('name', '=', 'FirebirD')
   //          ->orWhere(function ($query) {
   //              $query->where('id', '>', 50)
   //                    ->where('email', '<>', 'superadmin');
   //          })
   //          ->get();
			// dump($users);	
			// ->orWhere(function ($query){
			// 	$query->where
			// }))->get
	
			
			//	$user  = User::with('phone');//get data from 2 tables with id =1

			//echo $name = User::find(1)->getOriginal('email');

			//$user->phone;
			//dump($user);
			//$user->phone()->save();


		//Update child table
				// $id = 1;
				// $user = User::find($id);w
				// $user->save(); o
				// $user->phone->name = 'ZZZ A7';
				// $user->phone->save();

		//Delete
				//$user = User::find($id)->phone->delete();

	}


	public function searchPage()
	{
		return View('users.search');
	}

	public function find(Request $request)
	{
    		///$user = User::where('name', 'like', '%' . $request->get('name') . '%')->take(10)->get();
			//$user = User::whereRaw("MATCH(name) AGAINST(? IN BOOLEAN MODE)", array($request->get('name')))->get();
			//$user = User::whereRaw("MATCH(name) AGAINST(? IN NATURAL LANGUAGE MODE)", array($request->get('name')))->get();
			$user = User::whereRaw("MATCH(name) AGAINST(? WITH QUERY EXPANSION)", array($request->get('name')))->select('name')->take(10)->get();

// SELECT productName
// FROM products
// WHERE MATCH(productName) 
//       AGAINST('1992' WITH QUERY EXPANSION);


// SELECT productName, productline
// FROM products
// WHERE MATCH(productline) 
// AGAINST('Classic,Vintage' IN NATURAL LANGUAGE MODE)

// SELECT name
// FROM `users`
// WHERE MATCH(name) 
//       AGAINST('Việt' IN BOOLEAN MODE )

    	return response()->json($user);
	}
    
}//end
