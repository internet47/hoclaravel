<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ArticleService;
use App\Http\Requests\ArticleRequest;
use Illuminate\Support\Facades\Session;
use App\Article;
use App\Jobs\SendArticleEmail;
use PDF; // This is dompdf library
use App\Events\MyNotificationEvent;
use App\Events\ViewArticleEvent;

class ArticleController extends Controller
{

	protected $article;

	public function __construct(ArticleService $article)
	{
		$this->article = $article;
	}

	public function addArticle()
	{
		return view ('articles.add');
	}

	public function getCSRF()
	{
		$csrf = csrf_token();
		$data = [
			'status'=>true,
			'token'=>$csrf,
			];
		return response()->json($data);
	}

	public function index()
	{
		echo csrf_token();
		var_dump(Session::all());
		dump(\App\User::find(1007)->name);
		$articles = $this->article->getArticles();
		$articles = Article::orderBy('created_at','asc')->paginate(5);
		return view ('articles.index',compact('articles'));
	}

	public function createArticle(ArticleRequest $request)
	{
		
		$filename = $this->article->isUpload($request);
		if ($filename !='')
		{
			$article = $this->article->createArticle($request, $filename);
			$data = $request->all();
			$data['image_file']='';

			event(new MyNotificationEvent('Hệ thống vừa thêm bái viết mới có tựa đề là: '. $data['title']));
			$this->adddQueue($data);

			return redirect('/articles')->with(['status'=>'Articles saved']);
		}

		return redirect('/articles')->with(['status'=>'Articles upload failed']);

		
	}

	public function editArtile($id)
	{
		$article = $this->article->findArticle($id);
		if ($article){
			if (!empty($article->image_file))
				Session::put('image_file_old', $article->image_file);
		return view ('articles.edit')->with ('article',$article);
		}
		return redirect()->route('index.article')->with(['status'=>'Article was not found']);
			
	}

	public function updateArticle($id, ArticleRequest $request)
	{

		if ($request->hasFile('image_file'))
		{	
			$filename = $this->article->isUpload($request);
			
			if ($filename !='')
			{
				$request = $request->all();
				$request['image_file'] = $filename;
				$article               = $this->article->updateArticle($id, $request);
				return redirect('/articles')->with(['status' =>'Articles updated']);
			}
			return redirect('/articles')->back()->with(['status'=>'File upload is failed']);
		}
		else
			{
			$request = $request->all();

			$request['image_file'] =  Session::get ('image_file_old');
			//dd($request);
			$this->article->updateArticle($id, $request);
			$this->adddQueue($request);
			return redirect('/articles')->with(['status'=>'Articles updated with no image']);
		}

	}

	public function deleteArticle($id)
	{
		die();
		if (!empty($id) && is_numeric($id) && $this->article->deleteArticle($id) == TRUE)
			{
			return redirect('/articles')->with (['status'=>'Delete article successed']);	
			}
		else
			{
			return redirect('/articles')->with (['status'=>'Article is not exist']);	
			}
	}


	public function adddQueue(array $data)
	{
		try {
				$ArtileJob = new SendArticleEmail($data);
				$this->dispatch($ArtileJob);//Đưa job vào queue
			} catch (Exception $e) {
				throw new Exception('Can not add job into queue');
			}
	}
	
	/**
	 * Generate a Article via pdf
	 * @return [type] [description]
	 */
	public function viewPdf($id)
	{
		if (!empty($id) && is_numeric($id))
		{
			$article = $this->article->findArticle($id);	
			//view()->share('article', $article);
			 	return view('articles.pdf')->with('article',$article);
		}
		return redirect()->route('index.artile')->with(['status'=>'This article is not exist']);
		
	}


	public function downloadPdf($id)
	{
		$article = $this->article->findArticle($id);	
		view()->share('article', $article);
		$pdf = PDF::setOptions(['dpi' => 75, 'defaultFont' => 'Tahoma', 'adminUsername'=>'user', 'adminPassword'=>123]);
		$pdf->loadView('articles.pdf')->setPaper('a4', 'landscape')->setWarnings(false);//->save('myfile.pdf');
		           return $pdf->download('articles.pdf');//download
            //return $pdf->stream();//View on browser
	}

	public function showArticle($id)
	{
		$article =  $this->article->showArticle($id);
		$data = $article->toArray();

		event(new ViewArticleEvent($article)); // đưa event vào để gửi mail khi có người đọc bài viết này
		return View('articles.view')->with(['article'=>$article]);
	}


	
}