<?php

namespace App\Http\Controllers;

use App\Car;
use Illuminate\Http\Request;

class CarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cars= Car::orderBy('created_at','asc')->paginate(5);
        return view('mongo.index',compact('cars'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('mongo.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $car = new Car();
        $car->carcompany = $request->get('carcompany');
        $car->model= $request->get('model');
        $car->price=$request->get('price');
        $car->save();

        return redirect('car')->with('success','Bạn đã insert thành công');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function show(Car $car)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $car = Car::find($id);
        return View ('mongo.edit', compact('car', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $car= Car::find($id);
        $car->carcompany = $request->get('carcompany');
        $car->model = $request->get('model');
        $car->price = $request->get('price');        
        $car->save();
        return redirect('car')->with('success', 'Xe của bạn đã được update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $car = Car::find($id);
        $car->delete();
        return redirect('car')->with('success','Xe của bạn đã bị xóa');
    }

    public function search(Request $request)
    {
        $search_text = trim($request->get('search'));
        if (!empty($search_text))
        {
            $cars = Car::where('carcompany','LIKE','%'.$search_text.'%')->orWhere('model','LIKE','%'.$search_text.'%')->paginate(5);
            $total =  $cars->total();
            \Session::flash('success', 'Bạn đã tìm được  '.$total . ' dữ liệu');
            return view('mongo.index',compact('cars','total'));
        }
        return redirect('/car')->withSuccess('Vui lòng nhập từ khóa');
    }
}
