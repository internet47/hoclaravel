<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//Đưa 2 thư viện này vào
use Telegram\Bot\FileUpload\InputFile;
use Telegram\Bot\Laravel\Facades\Telegram;

class TelegramController extends Controller
{
  public function updatedActivity()
    {
        $activity = Telegram::getUpdates();
        dd($activity);
    }
 
    public function sendMessage()
    {
        return view('telegram.message');
    }
 
    public function storeMessage(Request $request)
    {
        $text = "A new contact us query\n"
            . "<b>Email Address: </b>\n"
            . "$request->email\n"
            . "<b>Message: </b>\n"
            . $request->message
            .' http://vnexpress.net';
 
        Telegram::sendMessage([
            'chat_id' => env('TELEGRAM_CHANNEL_ID', ''),
            'parse_mode' => 'HTML',
            'text' => $text
        ]);
 
        return redirect()->back();
    }
 
    public function sendPhoto()
    {
        return view('telegram.photo');
    }
 
    public function storePhoto(Request $request)
    {
       
         $data = $request->all();
         $path = $data['path'];
         $caption = $data['caption'];
 
        //Gửi link có preview bên Tele kèm caption
        Telegram::sendPhoto([
            'chat_id' => env('TELEGRAM_CHANNEL_ID', ''),
            'photo'   => $path ,
            'caption' => $caption,
        ]);

        //Gửi vị trí và sẽ hei63n thị bản đồ trên Telegram
        $response = Telegram::sendLocation([
          'chat_id' =>  env('TELEGRAM_CHANNEL_ID', ''),
          'latitude' => 37.7576793,
            'longitude' => -122.5076402,
        ]);

        $messageId = $response->getMessageId();

        return redirect()->back();
    }


}
