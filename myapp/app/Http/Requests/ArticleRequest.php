<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class ArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=>'required|min:3|unique:articles,title,'.$this->id,
            'short_content'=>'required|min:5|max:150',
            //'image_file'=>'image|mimes:jpg,jpeg,png,gif|max:2048',
            'user_id'=>'required|regex:/^[0-9]+$/',
        ];
    }

    public function messages()
     {
         return [
             'title.required' => 'Yêu cầu nhập nội dung cho title ',
             'title.unique' => 'Dữ nhiệu :attribute bạn nhập bị trùng. Vui lòng nhập lại.',
         ];
     }

     /**
      * Hàm trả về khi lỗi đối với request là API
      * @param  array  $errors [description]
      * @return [type]         [description]
      */
     public function response(array $errors)
     {
          if (Request::is('api/*'))
            return $this->sendError($errors,'Can not insert', 404); 
     }


     // Hàm trả về lỗi theo format
     public function sendError($error, $errormessage, $code = 404)
        {
            $response = array(
                    'success' => false,
                    'message' => $error
            );

            if (!empty($errormessage))
                $response['data'] = $errormessage;

            return response()->json($response, $code);
            
        }
    
}
