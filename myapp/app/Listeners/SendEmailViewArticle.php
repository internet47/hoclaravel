<?php

namespace App\Listeners;

use App\Events\ViewArticleEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
//Thêm thư viện Mail
use Mail;
use App\Mail\ViewArticleEmail;

class SendEmailViewArticle
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

     /*
     * Handle the event.
     *
     * @param  ViewArticleEvent  $event
     * @return void
     */
    public function handle(ViewArticleEvent $event)
    { 
        Mail::to('quocviet.inc@gmail.com')
        ->send(new ViewArticleEmail($event->article)); // lấy property article của event. Vì nó chứa 1 object của article
        \Storage::put('loginactivity.txt', 'bạn đang xem: '.$event->article);
    }
}
