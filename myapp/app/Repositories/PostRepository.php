<?php
namespace App\Repositories;
use App\Post;


/**
 * Create Repository for Post
 */
class PostRepository
{
	protected $post;

	public function __construct(Post $post)
	{
		$this->post = $post;
	}

	public function getAll()
	{
		return $this->post->all();
	}

	public function findID($id)
	{
		return $this->post->find($id);
	}

	public function update($id, array $attributes)
	{
		return $this->post->find($id)->update($attributes);
	}

	public function delete($id)
	{
		return $this->post->find($id)->delete();
	}

	public function create($attributes)
	 {
	    return $this->post->create($attributes);
	 }


}//end class

