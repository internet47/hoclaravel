<?php
namespace App\Repositories;
use App\Article;


class ArticleRepository
{

	protected $article;

	public function __construct(Article $article)
	{
		$this->article = $article;
	}


	public function getArticles()
	{
		return $this->article->orderBy('id', 'desc')->with('comments')->get();
		
	}

	public function createArticle(array $data, $filename)
	{
		$data['image_file'] = $filename;
		return $this->article->create($data);
	}

	public function findArticle($id)
	{
		return $this->article->find($id);
	}


	public function updateArticle($id, array $data)
	{
		return $this->article->find($id)->update($data);
	}


	public function deleteArticle($id)
	{
		return $this->article->find($id)->delete();
	}

	public function showArticle($id)
	{
		return $article =  $this->article->with('comments')->find($id);//with('comments')
	}




}//end 

