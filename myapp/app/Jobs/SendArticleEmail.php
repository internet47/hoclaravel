<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use Mail;
use App\Mail\ArticleEmail;
use Carbon\Carbon;

class SendArticleEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $email="viet.tq@tctav.com";
        $template = new ArticleEmail($this->data);
        $when = Carbon::now()->second(10);
            $cc='';
             Mail::to($email)->cc($email)->bcc($email)
                //->send($template);
               // ->queue($template);
                ->later($when, $template);
    }
}
