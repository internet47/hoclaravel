<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable; //khai báo Trait cho slug


class Article extends Model
{
	use Sluggable;// dùng trail của slug
    protected $fillable = ['title','slug','short_content', 'content', 'user_id', 'image_file']; //khai báo slug để tử động lưu

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title' // đây là cột dùng  để generate ra slug từ title
            ]
        ];
    }

    public function comments()
    {
    	return $this->hasMany('App\Comment');
    }
}
