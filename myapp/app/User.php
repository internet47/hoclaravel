<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\Newslack;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    //  protected static function boot()
    // {
    //     parent::boot();
    //     self::created(function($model) {
    //         $model->notify(new Newslack());
    //     });
    // }


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function phone()
    {
    	return $this->hasOne('App\Phone','user_id','id'); // user_id là foreign key sẽ refer tới id của bảng User
    }


    // function getNameAttribute($name='')
    // {
    // 	return strtoupper($name);
    // }

    public function getEmailAttribute($email)
    {
    	return strtoupper($email);
    }


    public function routeNotificationForSlack($notification = 'slack')
       {
           return env('SLACK_WEBHOOK_URL');
       }


    // public function routeNotificationForNexmo($notification = 'nexmo')
    // {
    //     return $this->phone;
    // }


        // public function getJWTIdentifier()
        // {
        //     return $this->getKey();
        // }
        // public function getJWTCustomClaims()
        // {
        //     return [];
        // }
}
