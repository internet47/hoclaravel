<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Telegram\Bot\Api;



// Auth::routes();//tương đu
Route::group(['middleware' => ['web']], function() {
	/// Authentication Routes...
	Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
	Route::post('login', 'Auth\LoginController@login');
	Route::post('logout', 'Auth\LoginController@logout')->name('logout');

	// Registration Routes...
	Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
	Route::post('register', 'Auth\RegisterController@register');

	// Password Reset Routes...
	Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
	Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
	Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
	Route::post('password/reset', 'Auth\ResetPasswordController@reset');
});//end middlware web


Route::get('items', 'ItemController@index');
Route::post('items/import', 'ItemController@import');
Route::post('items/import2', 'ItemController@importWithDifferenceHeader');
Route::get('items/export', 'ItemController@export');

//Route::group(['prefix' => 'admin'], function() {
    

Route::group(['middleware' => 'auth'], function() {

				Route::get('/','PostController@index' )->name('dashboard.post');
				Route::get('/posts','PostController@index' );
				Route::post('/post', 'PostController@createPost');
				Route::get('/post/{id}', 'PostController@editPost')->name('edit.post');
				Route::put('/post/{id}', 'PostController@updatePost')->name('update.post');
				Route::delete('/post/{id}', 'PostController@deletePost')->name('destroy.post');

				//	Article
				Route::get('/articles','ArticleController@index' )->name('index.article');
				Route::get('/article/add','ArticleController@addArticle' )->name('add.article');
				Route::post('/article', 'ArticleController@createArticle')->name('save.article');
				Route::get('/article/show/{id}','ArticleController@showArticle' )->name('show.article');
				Route::get('/article/{id}', 'ArticleController@editArtile')->name('edit.article');
				Route::post('/article/{id}', 'ArticleController@updateArticle')->name('update.article');
				Route::delete('/article/{id}', 'ArticleController@deleteArticle')->name('destroy.article');	
				Route::get('/articles/viewpdf/{id}','ArticleController@viewPdf' )->name('pdf.article');
				Route::get('/articles/downloadpdf/{id}','ArticleController@downloadPdf' )->name('downloadpdf.article');
				
				Route::get('comments/{id}', 'CommentController@showComment')->name('show.comment');
				Route::post('comments', 'CommentController@addComment')->name('add.comment');


				//Send mail
				Route::get('email-test', function(){
					try {
						$details['email'] = 'viet.tq@tctav.com';
			    		dispatch((new App\Jobs\SendEmailJob($details))->delay(10));
			    		dd('done');
					} catch (Exception $e) {
						throw new Exception('You have error when sending mail by queue');
					}
				});


				Route::get('qrcode', function () {
					//QrCode::format('png'); 
				    return base64_encode(QrCode::format('png')->generate('Example of QR code'));
				});

				Route::get('clear', function () {
					$cleared = Artisan::call('cache:clear');
					$cleared = Artisan::call('route:clear');
					$cleared = Artisan::call('count:user');
				    return 'ban đã xóa cache';
				});

				// Search nhanh typehead.js
				Route::get('users','UserController@index' )->name('user.index');
				Route::get('users/search','UserController@searchPage' )->name('user.search');
				
				Route::get('shownotification','NotificationController@showNotification')->name('notification.show');
				Route::get('sendnotification','NotificationController@sendNotification')->name('notification.send');
				Route::get('/pusher', function() {
			    	event(new App\Events\MyNotificationEvent('alo alo!'));
			    return "Event has been sent!";
				});//end pusher

				//Slack
				Route::get('/slack', function() {	
					 App\User::first()->notify(new App\Notifications\Newslack('Nội dung test'));
					 echo "A slack notification has been send";

			   		//Notification::send($user, new App\Notifications\Newslack('Hello bạn'));
			   		//echo "A slack notification has been send";
			   		
			   		
				});//end slack

				//SMS Nexmo
				Route::get('/sms', function() {
				    	$user = new App\User();
						$user->phone_number= '+84908532383';   // Don't forget specify country code.
						$user->notify(new App\Notifications\NewSMS('Nội dung test'));
						echo "A sms notification has been send";
				});//end sms

				//Telegram
				Route::get('/tele', function() {
				    	$user = new App\User();
						$user->notify(new App\Notifications\NewTelegram('Hello World '.' https://vnexpress.net'));
						echo "A telegram notification has been send";

				});//end telegram

				
				Route::get('/tele/sdk', function() {
					$telegram = new Api(env('TELEGRAM_BOT_TOKEN'), false); //true is synchronus, false is asynchronus
				    $response = $telegram->getMe();
							    $botId = $response->getId();
								$firstName = $response->getFirstName();
								$username = $response->getUsername();
					$ewebhook = $telegram->getWebhookUpdates();
				    var_dump(compact('response','botId','firstName','username', 'ewebhook'));
				});//end tele


				Route::get('/sdk','TelegramController@sendMessage');
				Route::post('/sdk/send-message', 'TelegramController@storeMessage');
				Route::get('/sdk/send-photo', 'TelegramController@sendPhoto');
				Route::post('/sdk/store-photo', 'TelegramController@storePhoto');
				Route::get('/sdk/updated-activity', 'TelegramController@updatedActivity');

				//token
				Route::get('/csrf', function() {
					echo Session::token();
					$data = [
						'success'=>true,
						'token'=>csrf_token(),
						'message'=>'Get token success'
						];

				    return response()->json($data, 200);
				});

				Route::get('/csrf/{token}', function($token) {
					echo Session::token();
					dump($token);
				});

		Route::get('/home', 'HomeController@index')->name('home');

		//Model Car for Mongodb
		Route::get('car/add','CarController@create');
		Route::post('car/add','CarController@store');
		Route::get('car','CarController@index');
		Route::get('car/edit/{id}','CarController@edit');
		Route::post('car/edit/{id}','CarController@update');
		Route::delete('car/{id}','CarController@destroy');
		Route::get('/car/search','CarController@search');



 });//end middleware auth



//});//end group admin







