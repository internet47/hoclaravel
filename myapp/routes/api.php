<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });



//API for Articles
Route::get('articles', 'API\ArticleAPIController@index');
Route::post('articles/store', 'API\ArticleAPIController@store');
Route::get('articles/show/{id}', 'API\ArticleAPIController@show');
Route::post('articles/update/{id}', 'API\ArticleAPIController@update');
Route::get('articles/delete/{id}', 'API\ArticleAPIController@delete');

Route::get('users/find','UserController@find' )->name('user.find');



Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



//Test JWT
Route::post('user/register', 'API\APIRegisterController@register');
Route::post('user/login', 'API\APILoginController@login');

Route::group(['middleware' => 'jwt.auth'], function() {
    Route::get('user/get', 'API\APILoginController@getUser');
});
				
