@extends('telegram.layout')
 
@section('content')
    <form action="{{ url('sdk/store-photo') }}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="form-group">
            <label>Image path</label>
            <input type="text" name="path" id="input" class="form-control">
            <label>Your caption</label>
            <input type="text" name="caption" id="input" class="form-control">
         
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
@endsection