@extends('dashboard')
@section('title', 'Add Comment')
@section('content')
            <h4 class="page-header">Add Comment </h4>
           
            <form class="form-vertical" role="form" method="post" action="{{route('add.comment')}}" enctype="multipart/form-data">
               {{csrf_field()}}
               <input type="hidden" name="article_id" value="{{ $article_id }}">
               <div class="form-group">
                  <textarea class="form-control" rows="5" name="content" class="form-control" placeholder="Content">{{ old('content') }}</textarea>
               </div>
              
               <div class="form-group">
                  <button type="submit" class="btn btn-info">Add Comment</button>
                  <a href="{{ URL::previous() }}">Go Back</a>
               </div>
            </form>
@endsection