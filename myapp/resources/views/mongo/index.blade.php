<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Index Page</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
  </head>
  <body>
    <div class="container">
    <br />
    @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ \Session::get('success') }}</p>
      </div><br />
     @endif

     <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                 <p>
                    <form action="{{ action('CarController@search') }}" method="GET" role="form">
                        <legend>Tìm kiếm</legend>

                        <div class="form-group">
                          <label for="">Nhập từ khóa</label>
                          <input type="text" name="search"class="form-control" id="search-text" placeholder="Input field">
                        </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </p> 
     </div>


    <p class="pull-right"><a href="{{ url('car/add') }}" class="btn btn-default">Thêm mới</a></p>
    <table class="table table-striped">
    <thead>
      <tr>
        <th>ID</th>
        <th>Company</th>
        <th>Model</th>
        <th>Price</th>
        <th>Created at</th>
        <th colspan="2">Action</th>
      </tr>
    </thead>
    <tbody>
      
      @foreach($cars as $car)
      <tr>
        <td>{{$car->id}}</td>
        <td>{{$car->carcompany}}</td>
        <td>{{$car->model}}</td>
        <td>{{$car->price}}</td>
        <td>{{$car->created_at}}</td>
        <td><a href="{{action('CarController@edit', $car->id)}}" class="btn btn-warning">Edit</a></td>
        <td>
          <form action="{{action('CarController@destroy', $car->id)}}" method="post">
            {{ csrf_field() }}
            <input name="_method" type="hidden" value="DELETE">
            <button class="btn btn-danger" type="submit">Delete</button>
          </form>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
<p class="pull-right">

  {{ $cars->appends(request()->query())->links()  }}</p>
  </div>
  </body>
</html>