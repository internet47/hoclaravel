
      <ol class="breadcrumb">
       <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
       <?php $segments = ''; ?>
       @foreach(Request::segments() as $segment)
           <?php $segments .= '/'.$segment; ?>
           <li>
               <a href="{{ $segments }}">{{$segment}}</a>
           </li>
       @endforeach
      </ol>

        <a href="/">Home</a> >                
        <?php $link = "" ?>
        @for($i = 1; $i <= count(Request::segments()); $i++)
        @if($i < count(Request::segments()) & $i > 0)
        <?php $link .= "/" . Request::segment($i); ?>
        <a href="<?= $link ?>">{{ ucwords(str_replace('-',' ',Request::segment($i)))}}</a> >
        @else {{ucwords(str_replace('-',' ',Request::segment($i)))}}
        @endif
        @endfor