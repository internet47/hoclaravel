@extends('dashboard')
@section('title', 'Add Article')
@section('content')
            <h4 class="page-header">Add article </h4>
            <form class="form-vertical" role="form" method="post" action="{{route('save.article')}}" enctype="multipart/form-data">
               {{csrf_field()}}

               <div class="form-group">
                  <input type="text" name="title" class="form-control" placeholder="Title" value="{{ old('title') }}">
               </div>
               <div class="form-group">
                  <textarea class="form-control" rows="5" name="short_content" class="form-control" placeholder="short content">{{ old('short_content') }}</textarea>
               </div>
               <div class="form-group">
                  <textarea class="form-control" rows="5" name="content" class="form-control" placeholder="Content">{{ old('content') }}</textarea>
               </div>
               <div class="form-group">
                  <input type="file" name="image_file" class="form-control">{{ old('image_file') }}
               </div>
               <div class="form-group">
                  <input type="text" name="user_id" class="form-control" placeholder="author" value="{{ old('user_id') }}">
               </div>
               <div class="form-group">
                  <button type="submit" class="btn btn-info">Add article</button>
                  <a href="{{ URL::previous() }}">Go Back</a>
               </div>
            </form>
@endsection