@extends('dashboard')
@section('title', 'Article Page')
@section('content')

      <h4 class="page-header">Article Page</h4>
            <a href="{{route('add.article')}}">Add article</a>
            <table class="table table-bordered">
               <thead>
                  <tr>
                     <th>ID</th>
                     <th>Title</th>
                     <th>Slug</th>
                     <th>Short Content </th>
                     <th>Image files</th>
                     <th>Author</th>
                     <th>Created at</th>
                     <th>Updated at</th>
                      <th>Edit</th>
                     <th>Delete</th>
                     <th>View</th>
                     <th>PDF</th>
                     <th>QR-code</th>
                     <th>Comment</th>
                  </tr>
               </thead>
               @foreach($articles as $article)
               <tbody>
                  <tr> 
                     <td>{{$article->id}}</td>
                     <td>{{ title_case($article->title) }}</td>
                     <td>{{ $article->slug }}</td>
                     <td>{{ str_limit($article->short_content, 6) }}</td>
                     <td><img src="{{ asset('uploads').DIRECTORY_SEPARATOR.$article->image_file }}" width="200" alt="{{$article->title}}"></td>
                      <td>{{$article->user_id}}</td>
                       <td>{{$article->created_at}}</td>
                        <td>{{$article->updated_at}}</td>
                     <form action="{{route('edit.article', $article->id)}}" method="GET">
                        <td>
                           <p data-placement="top" data-toggle="tooltip" title="Edit"><button class="btn btn-success btn-xs" ><span class="fa fa-pencil fa-fw"></span>Edit</button></p>
                        </td>
                     </form>
                     <form action="{{route('destroy.article', $article->id)}}" method="POST" class="delete">
                        {{csrf_field()}}
                        {{method_field('DELETE')}}
                        <td>
                           <p data-placement="top" data-toggle="tooltip" title="Delete"><button class="btn btn-danger btn-xs"  onclick="return confirm('Are you sure?')"><span class="fa fa-fw fa-trash"></span>Delete</button></p>
                        </td>
                        <td><a class="btn btn-info btn-xs" href="{{ route('show.article', $article->id) }}">View</a></td>
                         <td><a class="btn btn-info btn-xs" href="{{ route('pdf.article', $article->id) }}">PDF</a></td>
                     </form>
                     <td> 
                       {{--  <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->color(255,234,234)->size(100)->backgroundColor(255,55,0)->generate('A simple example of QR code'))!!} " alt="pc"> --}}
                     </td>
                     <td><a class="btn btn-info btn-xs" href="{{ route('show.comment', $article->id) }}">Add Comments  </a> {{ $article->comments->count() }}  </td>
                  </tr>
               </tbody>
               @endforeach
            </table>
            <p class="pull-right">{{ $articles->links() }}</p>

@endsection



