@extends('dashboard')
@section('title', 'Article Page')
@section('content')
<h1>Show detail</h1>
<div class="panel panel-default">
<!-- Table -->
	<table class="table">
		<tbody>
			<tr>

				<td>Title</td>
				<td><strong>{{ $article->title }}</strong></td>
			</tr>
			<tr>
				<td>Short Content</td>
				<td>{{ $article->short_content }}</td>
			</tr>
			<tr>
				<td>Content</td>
				<td>{{ $article->content }}</td>
			</tr>
			<tr>
				<td>Author</td>
				<td>{{ $article->user_id }}</td>
			</tr>
			<tr>
				<td>Image</td>
				<td>
					 @if(!empty($article->image_file))
						<img src="{{ asset('uploads').DIRECTORY_SEPARATOR.$article->image_file }}" alt="">
					 @endif
				</td>
			</tr>
		</tbody>
	</table>
</div>


@foreach ($article->comments()->get() as $comment)
	<div class="form-group">
		<span class="label label-default pull-right">{{ $comment->created_at }}</span>
	</div>
	<div class="form-group"><textarea class="form-control" rows="5" name="content" class="form-control" placeholder="Content" readonly="readonly" disabled="true">{{ $comment->content }}</textarea>
	</div>
@endforeach


<a href=" {{ route('index.article') }} ">Back to Homepage</a>



@endsection
