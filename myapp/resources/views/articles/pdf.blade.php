@extends('dashboard')
@section('title', 'View Article via PDF')
@section('content')

<table class="table table-bordered table-hover">
	<thead>
		<tr>
			<th>No</th>
			<th>Title</th>
			<th>Content</th>
			<th>Download</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>{{ $article->id }}</td>
			<td>{{ $article->title }}</td>
			<td>{{ $article->content }}</td>
			<td><a class="btn btn-info btn-xs" href="{{ route('downloadpdf.article',$article->id) }}">Download PDF</a></td>
		</tr>
	</tbody>
</table>

@endsection