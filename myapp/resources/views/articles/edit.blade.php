@extends('dashboard')
@section('title', 'Add Article')
@section('content')
            <h4 class="page-header">Edit article </h4>
            
             {{Session::get('image_file_old')}}

            <form class="form-vertical" role="form" method="post" url="{{route('update.article', $article->id)}}" enctype="multipart/form-data">
                {{csrf_field()}}
                {{method_field('POST')}}

               <div class="form-group">
                  <input type="text" name="title" class="form-control" placeholder="Title" value="{{ $article->title }}">
               </div>
               <div class="form-group">
                  <textarea class="form-control" rows="5" name="short_content" class="form-control" placeholder="short content">{{ $article->short_content }}</textarea>
               </div>
               <div class="form-group">
                  <textarea class="form-control" rows="5" name="content" class="form-control" placeholder="Content">{{ $article->content }}</textarea>
               </div>
               <div class="form-group">
                  <input type="file" name="image_file" class="form-control">
                  <img src="{{asset('uploads').DIRECTORY_SEPARATOR.$article->image_file }}" width="100">
                  {{ $article->image_file }}
               </div>
               <div class="form-group">
                  <input type="text" name="user_id" class="form-control" placeholder="author" value="{{ $article->user_id  }}">
               </div>
               <div class="form-group">
                  <button type="submit" class="btn btn-info">Update article</button>
                  <a href="{{ URL::previous() }}">Go Back</a>
               </div>
            </form>
@stop

@section('js')
    <script type="text/javascript">
      $(document).ready(function() {
         //alert("asdsadas");
      });
   </script>
@endsection




