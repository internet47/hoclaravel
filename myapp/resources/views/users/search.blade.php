<!DOCTYPE html>
<html lang="vi">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Tìm kiếm thông minh sử dụng Typeahead trong ứng dụng Laravel">
        <meta name="author" content="FirebirD ['www.allaravel.com']">
        <title>Typeahead</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
            

        <!-- Begin page content -->
        <div class="container">
            <div class="page-header">
                <h3>typeahead.js</h3>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <form class="form-inline typeahead">
                        <div class="form-group">
                            <input type="name" class="form-control search-input" id="name" autocomplete="off" placeholder="Nhập tên khách hàng" autofocus>
                        </div>
                        <button type="submit" class="btn btn-default">Tìm kiếm</button>
                    </form>
                </div>
            </div>
        </div>

        <footer class="footer">
            <div class="container">
                <p class="text-muted">Example in <a href="https://allaravel.com">allaravel.com</a></p>
            </div>
        </footer>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.11.1/typeahead.bundle.min.js"></script>
                
        <script>
        $(document).ready(function($) {
        var my_suggestion_class = new Bloodhound({
            remote: {
                url: 'http://localhost:8000/api/users/find?name=%QUERY%',
                filter: function(data) {
                    console.log(data);
                    return data;
                },
                wildcard: '%QUERY%'
            },
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),// name là tên field cần lấy ra
            queryTokenizer: Bloodhound.tokenizers.whitespace
        });

        my_suggestion_class.initialize();

        $(".search-input").typeahead({
            hint: true, // hiển thị gợi ý khi nhập liệu
            highlight: true,  //thêm thẻ <strong> vào các từ trùng khớp trong phần gợi ý. Mặc định là false.
            minLength: 2 //Số ký tự tối thiểu cần nhập khi tính năng gợi ý được bắt đầu, mặc định là 1.
        }, {
            source: my_suggestion_class.ttAdapter(),
            limit: 20, // khai báo số lượng record được show lúc suggestion
            name: 'name', // tên field cần lấy ra giống ở trên
            displayKey: 'name', // hiển thị suggestion trong ô textbox 
            templates: {
                empty: [
                    '<div class="list-group search-results-dropdown"><div class="list-group-item">Không có kết quả phù hợp.</div></div>'
                ],
                header: [
                    '<div class="list-group search-results-dropdown">'
                ],
                footer: [
                    '</div>'
                ],
                suggestion: function (data) {// kết quả trả về và định dạng lại dữ liệu khi ouput
                    console.log(data);
                    return '<a href="users/' + data.name + '" class="list-group-item">' + data.name + '</a>'
                }
            }
        });
        });//end ready
        </script>
    </body>
</html>