@extends('dashboard')
@section('title', 'Index page')
@section('content')
            <h4 class="page-header">Laravel Repositories and Services </h4>
            @foreach ($errors->all() as $error)
            <p class="alert alert-danger">{{ $error }}</p>
            @endforeach 
            @if (session('status'))
            <div class="alert alert-success">
               {{ session('status')}}
            </div>
            @endif
            <form class="form-vertical" role="form" method="post" action="{{route('update.post', $post->id)}}">
               {{csrf_field()}}
               {{method_field('PUT')}}
               <div class="form-group">
                  <input type="text" name="title" class="form-control" placeholder="Title" value="{{$post->title}}">
               </div>
               <div class="form-group">
                  <textarea class="form-control" rows="5" name="body" class="form-control" placeholder="Content">{{$post->body}}</textarea>
               </div>
               <div class="form-group">
                  <button type="submit" class="btn btn-info">Update Post</button>
               </div>
            </form>
@endsection