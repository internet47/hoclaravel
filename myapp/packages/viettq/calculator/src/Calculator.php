<?php

namespace Viettq\Calculator;

use Illuminate\Database\Eloquent\Model;

class Calculator extends Model
{
    protected $table = 'calculator';

    protected $fillable = [
        'name',
    ];
}
