<?php

namespace Viettq\Calculator;

use Illuminate\Support\ServiceProvider;

class CalculatorServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //Khai báo đường dẫn file route nội bộ của package
        //include __DIR__.'/routes.php';
        $this->loadRoutesFrom(__DIR__.'/routes.php');

        //Load migration
        $this->loadMigrationsFrom(__DIR__.'/migrations');

         //Khai báo thư mục chứa views
        $this->loadViewsFrom(__DIR__.'/views','calculator');

        // Copy từ thư mục views trong package sang thư mục resources/views/.... (nếu thư mục ko tồn tại nó sẽ tự tạo)
        $this->publishes([
            __DIR__.'/views' => base_path('resources/views/viettq/calculator'),
        ]);

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //Khai báo Controller
        $this->app->make('Viettq\Calculator\CalculatorController');

       
    }
}
