<?php

Route::get('calculator', function(){
	echo 'Hello from the calculator package!';
});

Route::get('add/{a}/{b}', 'Viettq\Calculator\CalculatorController@add');
Route::get('subtract/{a}/{b}', 'Viettq\Calculator\CalculatorController@subtract');